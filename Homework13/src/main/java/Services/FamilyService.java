
package Services;
import DAO.CollectionFamilyDao;
import Entitiy.*;

import java.time.Instant;
import java.time.LocalDate;
import java.time.Year;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

public class FamilyService {
    CollectionFamilyDao collectionFamilyDao;

    public FamilyService() {
        collectionFamilyDao = new CollectionFamilyDao();
    }

    public List<Family> getAllFamilies(){
        return  collectionFamilyDao.getAllFamilies();
    }

    public void displayAllFamilies(){
        List<Family> families = getAllFamilies();
        for (Family family: families) {
            System.out.println(family.toString());
        }
    }
    public void getFamiliesBiggerThan(int number){
        getAllFamilies().stream().filter(family -> family.countFamily()>number).peek(x-> System.out.println(x.toString()));

    }
    public void getFamiliesLessThan(int number){
        getAllFamilies().stream().filter(family -> family.countFamily()<number).peek(x-> System.out.println(x.toString()));

    }
    public int countFamiliesWithMemberNumber(int number){
        return  getAllFamilies().stream().filter(family -> family.countFamily()==number).collect(Collectors.toList()).size();
    }

    public  void createNewFamily(Human mother, Human father){
        collectionFamilyDao.create(new Family(mother, father));

    }
    public void deleteFamilyByIndex(int index){
        collectionFamilyDao.deleteFamily(index);
    }
    public Family adoptChild(Family family, Human child){
        for (int i = 0; i < getAllFamilies().size(); i++) {
            if(getAllFamilies().get(i).equals(family)){
                getAllFamilies().get(i).addChild(child);
            }
            return family;
        }
        throw  new RuntimeException("No family found");
    }

    public void deleteAllChildrenOlderThen(int number) {
        for (Family family: getAllFamilies()) {
            List<Human> collected=  family.getChildren().stream().filter(child-> Year.now().getValue()- Instant.ofEpochMilli(child.getBirthdate()).atZone(ZoneId.systemDefault()).toLocalDate().getYear() > number).collect(Collectors.toList());
            collected.forEach(family::deleteChild);
        }
    }

    public int count(){
        return  getAllFamilies().size();
    }
    public  Family getFamilyById(int index){
        return getAllFamilies().get(index);
    }
    public List<Pet> getPets(int index){
        return  getAllFamilies().get(index).getPet();
    }

    public boolean addPet(int index, Pet pet){
        Family fam =  getAllFamilies().get(index);
        fam.getPet().add(pet);
        getAllFamilies().set(index,fam);
        return true;
    }



    public Family bornChild(Family family, String feminine, String masculine){
        for (int i = 0; i < getAllFamilies().size(); i++) {
            if (getAllFamilies().get(i).equals(family)) {
                if (feminine.equals("")) {
                    getAllFamilies().get(i).addChild(new Human(masculine, family.getFather().getSurname(), LocalDate.now().format(DateTimeFormatter.ofPattern("dd/MM/yyyy"))+""));
                } else if (masculine.equals("")) {
                    getAllFamilies().get(i).addChild(new Human(feminine, family.getFather().getSurname(), LocalDate.now().format(DateTimeFormatter.ofPattern("dd/MM/yyyy"))+""));
                } else {
                    getAllFamilies().get(i).addChild(new Human(masculine, family.getFather().getSurname(), LocalDate.now().format(DateTimeFormatter.ofPattern("dd/MM/yyyy"))+""));
                    getAllFamilies().get(i).addChild(new Human(feminine, family.getFather().getSurname(), LocalDate.now().format(DateTimeFormatter.ofPattern("dd/MM/yyyy"))+""));
                }
            }
        }
        return  family;

    }
    public  void addFamily(Family family){
        collectionFamilyDao.create(family);
    }

    public  void createRandomFamily(){
        Random random = new Random();
        int family_number = random.nextInt(5)+1;
        for (int i = 0; i < family_number; i++) {
            Woman mother = new Woman(WomanNames.values()[random.nextInt(WomanNames.values().length - 1)].get(), "AAAA", LocalDate.now().minusYears(random.nextInt(60)).format(DateTimeFormatter.ofPattern("dd/MM/yyyy")) + "");
            Man father = new Man(ManNames.values()[random.nextInt(ManNames.values().length - 1)].get(), "AAAA", LocalDate.now().minusYears(random.nextInt(60)).format(DateTimeFormatter.ofPattern("dd/MM/yyyy")) + "");
            mother.setIq_level(random.nextInt(200));
            father.setIq_level(random.nextInt(200));
            Family family = new Family(mother, father);
            int childNumber = random.nextInt(4);
            for (int j = 0; j < childNumber; j++) {
                int flag = random.nextInt(2);
                if (flag == 0) {
                    Woman baby_girl = new Woman(WomanNames.values()[random.nextInt(WomanNames.values().length - 1)].get(), "AAA", LocalDate.now().minusYears(random.nextInt(20)).format(DateTimeFormatter.ofPattern("dd/MM/yyyy")) + "");
                    baby_girl.setIq_level(random.nextInt(200));
                    family.addChild(baby_girl);
                } else {
                    Man baby_boy = new Man(ManNames.values()[random.nextInt(ManNames.values().length - 1)].get(), "AAA", LocalDate.now().minusYears(random.nextInt(20)).format(DateTimeFormatter.ofPattern("dd/MM/yyyy")) + "");
                    family.addChild(baby_boy);
                    baby_boy.setIq_level(random.nextInt(200));
                }
            }
            collectionFamilyDao.create(family);
        }
    }

    public  Man createNewFather(String name, String surname, String year, int iq){
        return new Man(name, surname, year, iq);
    }
    public  Woman createNewMother(String name, String surname, String year, int iq){
        return new Woman (name, surname, year, iq);
    }
    public  void   createNewFamilyWithParameters(String m_name, String m_surname, String m_year, int m_iq,String f_name, String f_surname, String f_year, int f_iq){
         createNewFamily(createNewMother(m_name, m_surname, m_year, m_iq), createNewFather(f_name, f_surname, f_year, f_iq));
    }
public   void loadData(List<Family> families){
        collectionFamilyDao.loadData(families);
}
// test
}