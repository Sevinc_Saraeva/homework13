package Entitiy;

public class MenuItems {
    public static void showMainMenu(){
        System.out.println(" 1. Test data");
        System.out.println(" 2. Display the entire list of families");
        System.out.println(" 3. Display a list of families more than ... ");
        System.out.println("  4. Display a list of families less than ...");
        System.out.println("5. Calculate the number of families where the number of members is ...");
        System.out.println(" 6. Create a new family");
        System.out.println("7. Delete a family by its index in the general list");
        System.out.println("8. Edit a family by its index in the general list");
        System.out.println(" 9. Remove all children over the age of majority ");
        System.out.println("10. Exit ");
        System.out.println("Please, enter the number of command: ");
    }
}
