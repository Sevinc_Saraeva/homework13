package Entitiy;
import DAO.CollectionFamilyDao;

public class FnFamily {
    private static long serialVersionUID = 1L;

    public  static int getMaxId(CollectionFamilyDao dao){
        return  dao.getAllFamilies().stream()
                .map(b -> b.getIndex())
                .max((id1, id2)-> id2 - id1)
                .orElse(0);
    }
}