package Entitiy;

import java.io.Serializable;

public class Cat extends Pet implements Serializable {
    public Cat(String nickname ) {
        super(nickname);
        super.setSpecies(Species.CAT);
    }
    @Override
    public void respond() {
        System.out.println("Hello, owner. I am Cat. My name is " + super.getNickname() + ". I miss you! ");
    }
}