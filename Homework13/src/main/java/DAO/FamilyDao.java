package DAO;

import Entitiy.Family;

import java.util.List;

public interface FamilyDao {

    List<Family> getAllFamilies();
    Family getFamilyByIndex(int index);
    void deleteFamily(int index);
    void deleteFamily(Family family);
    void create(Family family);

}
