
package DAO;
import Entitiy.Family;
import java.io.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

public class CollectionFamilyDao implements FamilyDao {
    private File file;

    public CollectionFamilyDao() {
       // this.file = new File(this.getClass().getResource("/").getPath() +"/TextFiles/families.bin");
        this.file = new File("families.bin");
    }

    @Override
    public List<Family> getAllFamilies() {
        try (ObjectInputStream ois = new ObjectInputStream(new BufferedInputStream(new FileInputStream(file)))) {
            Object readed = ois.readObject();
            List<Family> families = (ArrayList<Family>) readed;
            return families;
        } catch (IOException | ClassNotFoundException ex) {
            return new ArrayList<>();
        }
    }

    @Override
    public Family getFamilyByIndex(int index) {
        return getAllFamilies().stream().filter(family -> family.getIndex() == index).findAny().orElseThrow(() -> new RuntimeException("Wrong index"));
    }

    @Override
    public void deleteFamily(int index) {
        List<Family> families = getAllFamilies().stream().filter(fam -> fam.getIndex() != index).collect(Collectors.toList());
        loadData(families);
    }

    @Override
    public void deleteFamily(Family family) {
        List<Family> families = getAllFamilies().stream().filter(fam -> !fam.equals(family)).collect(Collectors.toList());
        loadData(families);
    }

    public void loadData(Collection<Family> families) {
        try (ObjectOutputStream oos = new ObjectOutputStream(new BufferedOutputStream(new FileOutputStream(file)))) {
            oos.writeObject(families);
        } catch (IOException ex) {
            throw new RuntimeException("DAO:write:IOException", ex);
        }
    }

    @Override
    public void create(Family family) {
        Collection<Family> families = getAllFamilies();
        families.add(family);
        loadData(families);
    }
}
